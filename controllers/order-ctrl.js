const paginate = require('jw-paginate');
const createCsvWriter = require("csv-writer").createObjectCsvWriter;

const Order = require('../models/order-model')

createOrder = (req, res) => {
    const body = req.body

    // console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a order',
        })
    }

    const order = new Order(body)

    if (!order) {
        return res.status(400).json({ success: false, error: err })
    }

    // const now = new Date();
    // db.order.save( { date: now, offset: now.getTimezoneOffset() } );

    order
        .save()
        .then(() => {
            return res.status(201).json({
                success: true,
                id: order._id,
                message: 'Order created!',
            })
        })
        .catch(error => {
            return res.status(400).json({
                error,
                message: 'Order not created!',
            })
        })
}

createOrdersFromCSV = (req, res) => {
    const body = req.body

    console.log(body)

    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a order',
        })
    }

    if (Array.isArray(body)) {

        body.forEach(element => {
            const order = new Order(element)

            console.log(order)

            if (!order) {
                return res.status(400).json({ success: false, error: err })
            }

            order
                .save()
                .then(() => {
                    return
                    // res.status(201).json({
                    //     success: true,
                    //     id: order._id,
                    //     message: 'Order created!',
                    // })
                    console.log("Success")
                })
                .catch(error => {
                    // return res.status(400).json({
                    //     error,
                    //     message: 'Order not created!',
                    // })
                    console.log(error)
                })
        })


    } else {
        return console.log('Не завантажилось')
    }




    // if (!order) {
    //     return res.status(400).json({ success: false, error: err })
    // }

    // const now = new Date();
    // db.order.save( { date: now, offset: now.getTimezoneOffset() } );

    // order
    //     .save()
    //     .then(() => {
    //         return res.status(201).json({
    //             success: true,
    //             id: order._id,
    //             message: 'Order created!',
    //         })
    //     })
    //     .catch(error => {
    //         return res.status(400).json({
    //             error,
    //             message: 'Order not created!',
    //         })
    //     })
}

updateOrder = async (req, res) => {
    const body = req.body

    console.log(body)
    if (!body) {
        return res.status(400).json({
            success: false,
            error: 'You must provide a body to update',
        })
    }

    Order.findOne({ _id: req.params.id }, (err, order) => {
        console.log({ _id: req.params.id })

        console.log(order)

        if (err) {
            return res.status(404).json({
                err,
                message: 'Order not found!',
            })
        }
        order.user_email = body.user_email
        order.date = order.date
        order.value = body.value
        order.currency = body.currency
        order.status = body.status
        order
            .save()
            .then(() => {
                return res.status(200).json({
                    success: true,
                    id: order._id,
                    message: 'Order updated!',
                })
            })
            .catch(error => {
                return res.status(404).json({
                    error,
                    message: 'Order not updated!',
                })
            })
    })
}

deleteOrder = async (req, res) => {
    await Order.findOneAndDelete({ _id: req.params.id }, (err, order) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!order) {
            return res
                .status(404)
                .json({ success: false, error: `Order not found` })
        }

        return res.status(200).json({ success: true, data: order })
    }).catch(err => console.log(err))
}

getOrderById = async (req, res) => {
    await Order.findOne({ _id: req.params.id }, (err, order) => {
        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!order) {
            return res
                .status(404)
                .json({ success: false, error: `Order not found` })
        }
        return res.status(200).json({ success: true, data: order })
    }).catch(err => console.log(err))
}

getOrders = async (req, res) => {
    await Order.find({}, (err, orders) => {
        // console.log(orders)

        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!orders.length) {
            return res
                .status(404)
                .json({ success: false, error: `Order not found` })
        }
        return res.status(200).json({ success: true, data: orders })

    }).catch(err => console.log(err))
}

getOrdersPage = async (req, res) => {
    await Order.find({}, (err, orders) => {
        const page = parseInt(req.query.page) || 1;

        const pageSize = 5;
        const pager = paginate(orders.length, page, pageSize);

        const pageOfOrders = orders.slice(pager.startIndex, pager.endIndex + 1);

        return res.json({ pager, pageOfOrders})
    })

}


getOrderInCsvById = async (req, res) => {
    await Order.findOne({ _id: req.params.id }, (err, order) => {
       console.log(order)

        const nameOforder = order.user_email;

        const csvWriter = createCsvWriter({
            path: "Order.csv",
            header: [
                { id: "user_email", title: "user_email" },
                { id: "date", title: "date" },
                { id: "value", title: "value" },
                { id: "currency", title: "currency" },
                { id: "status", title: "status" }
            ]
        })

        const csvFile = csvWriter
                           .writeRecords(order)
                             .then(() => {
                                  console.log("csvWriter.csv successfully!")
                             }
                           )

        if (err) {
            return res.status(400).json({ success: false, error: err })
        }

        if (!order) {
            return res
                .status(404)
                .json({ success: false, error: `Order not found` })
        }


        return res.status(200).json({ success: true, data: csvFile })
    }).catch(err => console.log(err))
}

getOrdersReport = async (req, res) => {
    await Order.find({}, (err, orders) => {
        // console.log(orders)

        if (err) {
            return res.status(400).json({ success: false, error: err })
        }
        if (!orders.length) {
            return res
                .status(404)
                .json({ success: false, error: `Order not found` })
        }

        const ordersApproved = () => {
            if (Array.isArray(orders)) {
            return
            orders.find({status: 'approved'});
            }
        }

        const filterOrdersData = (ordersApproved) => {
            if (Array.isArray(ordersApproved)) {

            }
        }



    }).catch(err => console.log(err))
}




module.exports = {
    createOrder,
    updateOrder,
    deleteOrder,
    getOrders,
    getOrderById,
    getOrdersPage,
    getOrderInCsvById,
    createOrdersFromCSV,
    getOrdersReport
}