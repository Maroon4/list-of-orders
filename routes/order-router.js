const express = require('express');

const OrderCtrl = require('../controllers/order-ctrl');

const router = express.Router();

router.post('/order', OrderCtrl.createOrder);
router.post('/order/csv', OrderCtrl.createOrdersFromCSV);
router.put('/order/:id', OrderCtrl.updateOrder);
router.delete('/order/:id', OrderCtrl.deleteOrder);
router.get('/order/:id', OrderCtrl.getOrderById);
router.get('/orders', OrderCtrl.getOrders);
router.get('/home', OrderCtrl.getOrdersPage);
router.get('/order/download/:id', OrderCtrl.getOrderInCsvById);
router.get('/orders/report', OrderCtrl.getOrdersReport)

module.exports = router;