const mongoose = require('mongoose');
const Schema = mongoose.Schema;



const Order = new Schema(
    {
        user_email: { type: String, required: true },
        date: { type: Date, required: true },
        value: { type: Number, required: true },
        currency: { type: String, required: true },
        status: { type: String, required: true },
    },
    { timestamps: true },
);

module.exports = mongoose.model('orders', Order);